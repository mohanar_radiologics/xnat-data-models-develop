/*
 * xnat-data-models: org.nrg.xdat.om.base.BaseXnatHdscandata
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xdat.om.base;

import org.nrg.xdat.om.base.auto.AutoXnatHdscandata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

import java.util.Hashtable;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseXnatHdscandata extends AutoXnatHdscandata {

	public BaseXnatHdscandata(ItemI item)
	{
		super(item);
	}

	public BaseXnatHdscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatHdscandata(UserI user)
	 **/
	public BaseXnatHdscandata()
	{}

	public BaseXnatHdscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
